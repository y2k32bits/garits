<?php require_once('Connections/godaddy.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_update"])) && ($_POST["MM_update"] == "form2")) {
  $updateSQL = sprintf("UPDATE User SET name=%s, type=%s WHERE userID=%s",
                       GetSQLValueString($_POST['name'], "text"),
                       GetSQLValueString($_POST['role'], "text"),
                       GetSQLValueString($_POST['hiddenField'], "int"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($updateSQL, $godaddy) or die(mysql_error());
}
$filter=$_POST["userName"];
if ($filter !=""){
	$where = 'LIKE "%'.$filter.'%"';
}
else
{
	$where="-1";
}
//echo $where;
mysql_select_db($database_godaddy, $godaddy);
$query_users = "SELECT * FROM `User` where name ".$where;
//echo $query_users;
$users = mysql_query($query_users, $godaddy) or die(mysql_error());
$row_users = mysql_fetch_assoc($users);
$totalRows_users = mysql_num_rows($users);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GARIS - Search User</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
  <div class="sidebar1">
	<?php include "adminNavigation.php"?>
  <!-- end .sidebar1 --></div>
<div class="content">

<form id="form1" name="filter" method="post" action="">
<div>
  <span id="sprytextfield1">
  	<label for="userName">Username:</label>
  	<input type="text" name="userName" id="userName" />
	<span class="textfieldRequiredMsg">A value is required.</span></span>
  	<input type="submit" name="submit" id="submit" value="filter" />
</div>  
</form>
  
<br />

<div class="users">
<table width="100%" border="1" cellspacing="1" cellpadding="1">
  <tr>
    <td>Name</td>
    <td>Username</td>
    <td>Role</td>
    <td>Edit</td>
  </tr>
  <?php do { ?>
    <tr>
      <form id="form2" name="form2" method="POST" action="<?php echo $editFormAction; ?>">
        <td>
          <span id="sprytextfield2">
            <label for="name"></label>
            <input name="name" type="text" id="name" value="<?php echo $row_users['name']; ?>" />
            <span class="textfieldRequiredMsg">A value is required.</span></span>
        </td>
        <td><?php echo $row_users['login']; ?></td>
        <td><label for="role"></label>
          <select name="role" id="role">
            <option value="receptionist" <?php if (!(strcmp("receptionist", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Receptionist</option>
            <option value="foreperson" <?php if (!(strcmp("fourperson", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Fourperson</option>
            <option value="manager" <?php if (!(strcmp("manager", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Manager</option>
            <option value="franchisee" <?php if (!(strcmp("franchisee", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Franchisee</option>
            <option value="admin" <?php if (!(strcmp("admin", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Admin</option>
            <option value="mechanic" <?php if (!(strcmp("mechanic", $row_users['type']))) {echo "selected=\"selected\"";} ?>>Mechanic</option>
          </select></td>
        <td><input name="hiddenField" type="hidden" id="hiddenField" value="<?php echo $row_users['userID']; ?>" />
          <input type="submit" name="Edit" id="Edit" value="Submit" /></td>
        <input type="hidden" name="MM_update" value="form2" />
      </form>
    </tr>
    <?php } while ($row_users = mysql_fetch_assoc($users)); ?>
</table>
<!-- end.users --></div>
<!-- end .content --></div>
<div class="sidebar2">
<div>
</div>
<!-- end .sidebar2 --></div>
<!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
</script>
</body>
</html>
<?php
mysql_free_result($users);
?>
