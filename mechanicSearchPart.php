
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/
SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
	<div class="sidebar1">
    <?php include "mechanicNavigation.php"?>
	<!-- end .sidebar1 --></div>
	<div class="content">
<form id="form1" name="filter" method="post" action="">
  <span id="sprytextfield1">
  <label for="partName">Part Name</label>
  <input type="text" name="partName" id="partName" />
  <span class="textfieldRequiredMsg">A value is required.</span></span>
  <input type="submit" name="submit" id="submit" value="filter" />
</form>

<br />

<form action="parts" method="get" name="parts">
<div>
<table width="100%" border="1" cellspacing="1" cellpadding="1">
  <tr>
    <th scope="col">Part Name</th>
    <th scope="col">Amount</th>
    <th scope="col">Actions</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</form>
  <!-- end .content --></div>
	<div class="sidebar2">
	<!-- end .sidebar2 --></div>
<!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
</script>
</body>
</html>
