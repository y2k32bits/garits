<?php require_once('Connections/godaddy.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "CreateCustomer")) {
  $insertSQL = sprintf("INSERT INTO Customer (name, phone, address) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['customerName'], "text"),
                       GetSQLValueString($_POST['customerPhone'], "text"),
                       GetSQLValueString($_POST['customerAddress'], "text"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($insertSQL, $godaddy) or die(mysql_error());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
	<div class="sidebar1">
	<!-- end .sidebar1 --></div>
	<div class="content">
    <form action="<?php echo $editFormAction; ?>" method="POST" name="CreateCustomer">
    <div>
      <span id="sprytextfield1">
      <label for="customerName">Customer Name</label>
      <input type="text" name="customerName" id="customerName" />
      <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
      <span id="sprytextfield2">
      <label for="customerPhone">Phone Number</label>
      <input type="text" name="customerPhone" id="customerPhone" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> 
    </div>
    <div>
      <span id="sprytextfield3">
      <label for="customerAddress">Address</label>
      <input type="text" name="customerAddress" id="customerAddress" />
      <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <input name="add" type="submit" value="Create" />
    <input name="cancel" type="reset" value="Cancel" />
    <input type="hidden" name="MM_insert" value="CreateCustomer" />
    </form> 
	<!-- end .content --></div>
	<div class="sidebar2">
	<!-- end .sidebar2 --></div>
<!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
</script>
</body>
</html>
