<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GARTIS - New Customer</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
  <div class="sidebar1">
      <ul class="nav">
      <li><a href="#">Home</a></li>
      <li><a href="#">Link two</a></li>
      <li><a href="#">Link three</a></li>
      <li><a href="#">Link four</a></li>
    </ul>
    <!-- end .sidebar1 --></div>
    
  <div class="content">
	<form action="/html/tags/html_form_tag_action.cfm" method="get">
	<fieldset>
	<legend>Your Details</legend>
	<div>
	<label for="first_name">First Name:</label><br>
	<input type="text" name="first_name" value="" maxlength="100" /><br>
	</div>
	<div>
	<label for="lunch">Lunch:</label><br>
	<input type="radio" name="lunch" value="pasta" /> Pasta
	<input type="radio" name="lunch" value="fish" /> Fish
	</div>
	<div>
	<label for="drinks">Drinks:</label><br>
	<input type="checkbox" name="drinks" value="beer" /> Beer
	<input type="checkbox" name="drinks" value="wine" /> Wine
	</div>
	<div>
	<label for="role">Role:</label><br>
	<select name="role">
	  <option value ="manager">manager</option>
	  <option value ="receptionist">receptionist</option>
  	<option value ="admin">admin</option>	
  	<option value ="mechanic">mechanic</option>
	</select>
	</div>
	<div>
	<label for="comments">Comments:</label><br>
	<textarea rows="3" cols="20" name="comments"></textarea>
	</div>
	<div>
	<input type="submit" value="Submit" />
	</div>
	</fieldset>
	</form> 
  <!-- end .content --></div>
  
  <div class="sidebar2">
  <!-- end .sidebar2 --></div>
<!-- end .container --></div>
  
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1")</script>
</body>
</html>
