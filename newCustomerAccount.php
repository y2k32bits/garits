<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GARTIS - New Customer</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
  <div class="sidebar1">
      <ul class="nav">
      <li><a href="#">Home</a></li>
      <li><a href="#">Link two</a></li>
      <li><a href="#">Link three</a></li>
      <li><a href="#">Link four</a></li>
    </ul>
    <!-- end .sidebar1 --></div>
  <div class="content">
  <form action="newCustomerAccount" method="post">
  	<div>
    <span id="sprytextfield1">
    <label for="customerName">Customer Name</label>
    <input type="text" name="customerName" id="customerName" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield2">
    <label for="phoneNumber">Phone Number</label>
    <input type="text" name="phoneNumber" id="phoneNumber" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield3">
    <label for="address">Address</label>
    <input type="text" name="address" id="address" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield4">
    <label for="postCode">Post Code</label>
    <input type="text" name="postCode" id="postCode" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="spryselect1">
    <label for="discount">Discount Plan</label>
    <select name="discount" id="discount">
    </select>
    <span class="selectRequiredMsg">Please select an item.</span></span>
    </div>
    <div>
    <input name="addCustomer" type="button" value="Add New Customer" />
    <input name="cancel" type="button" value="Cancel" />
    </div>
  </form>  
    <!-- end .content --></div>
  <div class="sidebar2">
    <!-- end .sidebar2 --></div>
  <!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1");
</script>
</body>
</html>
