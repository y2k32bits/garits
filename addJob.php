<?php require_once('Connections/godaddy.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "addJob")) {
  $insertSQL = sprintf("INSERT INTO Customer (name, phone, address) VALUES (%s, %s, %s)",
                       GetSQLValueString($_POST['customerName'], "text"),
                       GetSQLValueString($_POST['customerPhone'], "int"),
                       GetSQLValueString($_POST['customerAddress'], "text"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($insertSQL, $godaddy) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "addJob")) {
  $insertSQL = sprintf("INSERT INTO Car (regNo, make, model, colour, serialNumberEngin, vehicleChassis) VALUES (%s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['carReg'], "text"),
                       GetSQLValueString($_POST['make'], "text"),
                       GetSQLValueString($_POST['model'], "text"),
                       GetSQLValueString($_POST['colour'], "text"),
                       GetSQLValueString($_POST['serial'], "int"),
                       GetSQLValueString($_POST['chassis'], "int"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($insertSQL, $godaddy) or die(mysql_error());
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "addJob")) {
  $insertSQL = sprintf("INSERT INTO Job (jobNotes) VALUES (%s)",
                       GetSQLValueString($_POST['jobDescription'], "text"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($insertSQL, $godaddy) or die(mysql_error());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextarea.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationTextarea.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
	<div class="sidebar1">
	<!-- end .sidebar1 --></div>
	<div class="content">
    <form action="<?php echo $editFormAction; ?><?php echo $editFormAction; ?>" method="POST" name="addJob">
    <div>
      <span id="sprytextfield1">
      <label for="customerName">Customer Name</label>
      <input type="text" name="customerName" id="customerName" />
      <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
      <span id="sprytextfield2">
      <label for="customerAddress">Customer Address</label>
      <input type="text" name="customerAddress" id="customerAddress" />
      <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
   	<div> 
      <span id="sprytextfield3">
      <label for="customerPhone">Customer Phone</label>
      <input type="text" name="customerPhone" id="customerPhone" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div> 
       <span id="sprytextfield4">
       <label for="carReg">Vehicle Registration Number</label>
       <input type="text" name="carReg" id="carReg" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div>
       <span id="sprytextfield5">
       <label for="make">Make</label>
       <input type="text" name="make" id="make" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div>
       <span id="sprytextfield6">
       <label for="chassis">Chassis</label>
       <input type="text" name="chassis" id="chassis" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div>
       <span id="sprytextfield7">
       <label for="colour">Colour</label>
       <input type="text" name="colour" id="colour" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div>
       <span id="sprytextfield8">
       <label for="serial">Serial</label>
       <input type="text" name="serial" id="serial" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
     <div>
       <span id="sprytextfield9">
       <label for="model">Model</label>
       <input type="text" name="model" id="model" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
      <div>
      <span id="sprytextarea8">
      <label for="jobDescription">Job Description</label>
      <textarea name="jobDescription" id="jobDescription" cols="45" rows="5"></textarea>
      <span class="textareaRequiredMsg">A value is required.</span></span>
      <input name="submit" type="submit" value="Submit" />
     <input name="cancel" type="reset" value="Cancel" />
     <input type="hidden" name="MM_insert" value="addJob" />
    </form>  
	<!-- end .content --></div>
	<div class="sidebar2">
	<!-- end .sidebar2 --></div>
<!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextarea1 = new Spry.Widget.ValidationTextarea("sprytextarea1");
var sprytextarea2 = new Spry.Widget.ValidationTextarea("sprytextarea2");
var sprytextarea3 = new Spry.Widget.ValidationTextarea("sprytextarea3");
var sprytextarea4 = new Spry.Widget.ValidationTextarea("sprytextarea4");
var sprytextarea5 = new Spry.Widget.ValidationTextarea("sprytextarea5");
var sprytextarea6 = new Spry.Widget.ValidationTextarea("sprytextarea6");
var sprytextarea7 = new Spry.Widget.ValidationTextarea("sprytextarea7");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7");
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8");
var sprytextfield9 = new Spry.Widget.ValidationTextField("sprytextfield9");
var sprytextarea8 = new Spry.Widget.ValidationTextarea("sprytextarea8");
</script>
</body>
</html>
