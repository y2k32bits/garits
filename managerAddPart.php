<?php require_once('Connections/godaddy.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "") 
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;    
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$editFormAction = $_SERVER['PHP_SELF'];
if (isset($_SERVER['QUERY_STRING'])) {
  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
}

if ((isset($_POST["MM_insert"])) && ($_POST["MM_insert"] == "form")) {
  $insertSQL = sprintf("INSERT INTO Part (name, code, price, `year`, stockThreshold, vehicleType, manufacturer, stockNumber) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
                       GetSQLValueString($_POST['name'], "text"),
                       GetSQLValueString($_POST['code'], "text"),
                       GetSQLValueString($_POST['price'], "double"),
                       GetSQLValueString($_POST['year'], "int"),
                       GetSQLValueString($_POST['stockthreshold'], "int"),
                       GetSQLValueString($_POST['vehicle'], "text"),
                       GetSQLValueString($_POST['manufacturer'], "text"),
                       GetSQLValueString($_POST['stockLevel'], "int"));

  mysql_select_db($database_godaddy, $godaddy);
  $Result1 = mysql_query($insertSQL, $godaddy) or die(mysql_error());
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>GARTIS - New Part</title>
<link href="_css/main.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 7]>
<style>
.content { margin-right: -1px; } /* this 1px negative margin can be placed on any of the columns in this layout with the same corrective effect. */
ul.nav a { zoom: 1; }  /* the zoom property gives IE the hasLayout trigger it needs to correct extra whiltespace between the links */
</style>
<![endif]-->
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
  <div class="sidebar1">
  <?php include "managerNavigation.php"?>
  <!-- end .sidebar1 --></div>
  <div class="content">
  <form name="form" action="<?php echo $editFormAction; ?>" method="POST">
  <div>
    <span id="sprytextfield1">
    <label for="name">Name</label>
    <input type="text" name="name" id="name" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield2">
    <label for="code">Code</label>
    <input type="text" name="code" id="code" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield3">
    <label for="price">Price</label>
    <input type="text" name="price" id="price" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield4">
    <label for="year">Year</label>
    <input type="text" name="year" id="year" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield5">
    <label for="vehicle">Vehicle</label>
    <input type="text" name="vehicle" id="vehicle" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield6">
    <label for="stockthreshold">Stock Threshold </label>
    <input type="text" name="stockthreshold" id="stockthreshold" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
    <span id="sprytextfield7">
    <label for="manufacturer">Manufacturer</label>
    <input type="text" name="manufacturer" id="manufacturer" />
    <span class="textfieldRequiredMsg">A value is required.</span></span>
    </div>
    <div>
      <span id="sprytextfield8">
      <label for="stockLevel">Amount</label>
      <input type="text" name="stockLevel" id="stockLevel" />
      <span class="textfieldRequiredMsg">A value is required.</span></span> </div>
    <input name="addPart" type="submit" value="Add Part" />
    <input name="cancel" type="reset" value="Cancel" />
    <input type="hidden" name="MM_insert" value="form" />
  </form>
<!-- end .content --></div>
<div class="sidebar2">
    <!-- end .sidebar2 --></div>
<!-- end .container --></div>
<script type="text/javascript">
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1");
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2");
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3");
var sprytextfield4 = new Spry.Widget.ValidationTextField("sprytextfield4");
var sprytextfield5 = new Spry.Widget.ValidationTextField("sprytextfield5");
var sprytextfield6 = new Spry.Widget.ValidationTextField("sprytextfield6");
var sprytextfield7 = new Spry.Widget.ValidationTextField("sprytextfield7");
var sprytextfield8 = new Spry.Widget.ValidationTextField("sprytextfield8");
</script>
</body>
</html>
